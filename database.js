const sqlite3 = require('sqlite3').verbose();
const db = new sqlite3.Database(':memory:');

db.serialize(() => {
  db.run("CREATE TABLE IF NOT EXISTS guests (name TEXT)", err => {
    if (err) {
      console.error("Erro ao criar tabela:", err.message);
    }
  });
});

module.exports = db;

