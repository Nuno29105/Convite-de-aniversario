const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const cors = require('cors');
const db = require('./database');

const app = express();
const PORT = 4000; // Porta alterada para 4000

app.use(bodyParser.json());
app.use(cors());
app.use(express.static(path.join(__dirname)));

// Servir o arquivo HTML
app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, 'index.html'));
});

app.get('/api/guests', (req, res) => {
  db.all("SELECT name FROM guests", (err, rows) => {
    if (err) {
      console.error("Erro ao recuperar convidados:", err.message);
      res.status(500).json({ error: "Erro ao recuperar convidados" });
    } else {
      res.json({ guests: rows });
    }
  });
});

app.post('/api/confirm', (req, res) => {
  const rsvp = req.body;
  if (!rsvp.name) {
    return res.status(400).json({ error: 'Nome é obrigatório' });
  }
  db.run("INSERT INTO guests (name) VALUES (?)", [rsvp.name], function(err) {
    if (err) {
      console.error("Erro ao inserir convidado:", err.message);
      res.status(500).json({ error: "Erro ao inserir convidado" });
    } else {
      res.status(201).json({ name: rsvp.name });
    }
  });
});

app.listen(PORT, () => {
  console.log(`Server is running on http://localhost:${PORT}`);
});




